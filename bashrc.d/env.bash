function add_to_path() {
	if ! [[ "$PATH" =~ "$1:" ]]; then
		PATH="$1:$PATH"
	fi
	export PATH
}

function add_to_pkg_config_path() {
	if ! [[ "$PKG_CONFIG_PATH" =~ "$1:" ]]; then
		PKG_CONFIG_PATH="$1:$PKG_CONFIG_PATH"
	fi
	export PKG_CONFIG_PATH
}

function add_to_ld_library_path() {
	if ! [[ "$LD_LIBRARY_PATH" =~ "$1:" ]]; then
		LD_LIBRARY_PATH="$1:$LD_LIBRARY_PATH"
	fi
	export LD_LIBRARY_PATH
}

export LOCAL="$HOME/local"

add_to_path "$LOCAL/bin"
add_to_ld_library_path "$LOCAL/lib"
add_to_ld_library_path "$LOCAL/lib64"
add_to_pkg_config_path "$LOCAL/share/pkgconfig"
add_to_pkg_config_path "$LOCAL/lib/pkgconfig"
add_to_pkg_config_path "$LOCAL/lib64/pkgconfig"

#!/usr/bin/env bash

mkdir -p ~/.bashrc.d
ln -sf ~/git/dotfiles-torino/bashrc.d/* ~/.bashrc.d/
